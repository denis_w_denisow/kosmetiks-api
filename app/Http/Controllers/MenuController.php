<?php

namespace App\Http\Controllers;

use App\Models\MenuSetting;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function get() {
      return MenuSetting::all();
    }
}
