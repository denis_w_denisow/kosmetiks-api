<?php

namespace Database\Seeders;

use App\Models\MenuSetting;
use Illuminate\Database\Seeder;

class MenuSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MenuSetting::create([
          'id' => 1,
          'link' => '/',
          'name' => 'главная',
          'access' => 'user',
        ]);
        MenuSetting::create([
          'id' => 2,
          'link' => '/discounts',
          'name' => 'скидки',
          'access' => 'user',
        ]);
        MenuSetting::create([
          'id' => 3,
          'link' => '/delivery',
          'name' => 'доставка',
          'access' => 'user',
        ]);
        MenuSetting::create([
          'id' => 4,
          'link' => '/contacts',
          'name' => 'контакты',
          'access' => 'user',
        ]);
        MenuSetting::create([
          'id' => 5,
          'link' => '/articles',
          'name' => 'статьи',
          'access' => 'user',
        ]);
        MenuSetting::create([
          'id' => 6,
          'link' => '/feedback',
          'name' => 'отзывы',
          'access' => 'user',
        ]);
        MenuSetting::create([
          'id' => 7,
          'link' => '/basket',
          'name' => 'корзина',
          'access' => 'user',
        ]);
        MenuSetting::create([
          'id' => 8,
          'link' => '/orders',
          'name' => 'заказы',
          'access' => 'user',
        ]);
        MenuSetting::create([
          'id' => 9,
          'link' => '/admin',
          'name' => 'настройки',
          'access' => 'admin',
        ]);
    }
}
